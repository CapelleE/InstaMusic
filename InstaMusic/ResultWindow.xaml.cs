﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InstaMusic
{
    /// <summary>
    /// Interaction logic for ResultWindow.xaml
    /// </summary>
    public partial class ResultWindow : Window
    {
        private readonly string searchQuery = "";
        private MusicFinder musicFinder;
        private FoundSongMeta songInfo;

        public ResultWindow(string searchQuery)
        {
            InitializeComponent();
            // init vars
            this.searchQuery = searchQuery;
            
            // init music finder (not started yet!)
            musicFinder = new MusicFinder(this.searchQuery);
            
            // Modify labels on "searching" mode
            setSearchMode();
            this.Show();
            startSearching();
        }

        public void startSearching()
        {
            songInfo = musicFinder.doSearch();
            setFoundMode(songInfo);
        }

        private void setSearchMode()
        {
            this.Title = "Searching...";
            gridFoundMode.Visibility = Visibility.Hidden;
            gridSearchMode.Visibility = Visibility.Visible;
            lblSearching.Content = "Searching for:\n" + searchQuery;
        }

        private void setFoundMode(FoundSongMeta foundInfos)
        {
            if (foundInfos.searchSuccess)
                this.Title = "Found something!";
            else
                this.Title = "Nothing found";
            gridFoundMode.Visibility = Visibility.Visible;
            gridSearchMode.Visibility = Visibility.Hidden;

            if(foundInfos.searchSuccess)
            {
                lblFoundSongTitle.Content = foundInfos.title;
                gridFoundSongControls.Visibility = Visibility.Visible;
                gridSongNotFoundControls.Visibility = Visibility.Hidden;
            }
            else
            {
                lblFoundSongTitle.Content = "Found nothing";
                gridFoundSongControls.Visibility = Visibility.Hidden;
                gridSongNotFoundControls.Visibility = Visibility.Visible;
            }
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            musicFinder.doDownloadAndConvert(songInfo);
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

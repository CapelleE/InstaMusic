﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using HtmlAgilityPack;
using System.Net;
using YoutubeExtractor;

//TODO: Clean up the code / Break down into more classes for clarity sakes.

namespace InstaMusic
{
    /// <summary>
    /// Class representing the metadata of a song found on youtube.
    /// </summary>
    public class FoundSongMeta
    {
        public string title;
        public string fullDownloadLink;
        public bool searchSuccess;

        public FoundSongMeta()
        {
            title = "";
            fullDownloadLink = "";
            searchSuccess = false;
        }
    }

    /// <summary>
    /// Class in charge of taking the user's search query, parsing it, downloading the youtube video and converting it to mp3.
    /// </summary>
    public class MusicFinder
    {
        private readonly string songSearchQuery;
        private readonly string BASE_URL = "http://www.youtube.com/";
        private readonly string BASE_SEARCH_QUERY_URL = "http://www.youtube.com/results?search_query=";
        private HttpWebRequest request;
        private string realUrl = "";

        public MusicFinder(string songSearchQuery)
        {
            this.songSearchQuery = songSearchQuery;
            realUrl = BASE_SEARCH_QUERY_URL + makeURLYoutubeReadable(songSearchQuery);
            request = (HttpWebRequest)WebRequest.Create(realUrl);
        }

        /// <summary>
        /// The GUI layer can call this function to retrieve the name of the found song.
        /// GUI can then ask for a confirmation to the user.
        /// </summary>
        /// <returns>The name of the song found on Youtube.</returns>
        public FoundSongMeta doSearch()
        {
            return findSongOnYoutube();
        }

        public void doDownloadAndConvert(FoundSongMeta info)
        {
            downloadAndConvertSong(info);
        }

        /// <summary>
        /// Replaces spaces with pluses ('+') so youtube understands the search query.
        /// </summary>
        /// <param name="input">String to transform.</param>
        /// <returns>Transformed input string.</returns>
        private string makeURLYoutubeReadable(string input)
        {
            input.Replace(' ', '+');
            return input;
        }

        /// <summary>
        /// Attempts to find the song corresponding to the search query, on Youtube.
        /// </summary>
        /// <returns>The string of the most relevant video, or empty string if nothing was found.</returns>
        private FoundSongMeta findSongOnYoutube()
        {
            FoundSongMeta result = new FoundSongMeta();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = null;
            StreamReader rsReader = null;

            if(response.StatusCode == HttpStatusCode.OK)
            {
                responseStream = response.GetResponseStream();
                rsReader = new StreamReader(responseStream);

                //TODO: Check encodings!

                string htmlData = rsReader.ReadToEnd();
                result = parseHtmlDataIntoSongTitle(htmlData);

                responseStream.Close();
                rsReader.Close();
            }
            else
            {
                // Return "nothing found"
                // var "result" should still be on "nothing found" status
            }

            return result;
        }

        /// <summary>
        /// Creates an HTML document, then parses it to find the most relevant youtube link the user desires.
        /// </summary>
        /// <param name="input">The HTML page under the form of a string.</param>
        /// <returns>The found song metadata (title, download url and whether the search was a success or not)</returns>
        private FoundSongMeta parseHtmlDataIntoSongTitle(string input)
        {
            FoundSongMeta result = new FoundSongMeta();

            // Transform our string into a DOM
            HtmlDocument documentRoot = new HtmlDocument();
            documentRoot.LoadHtml(input);

            // "input" is now parseable
            // 1) find all links in the html document ***and select the first*** that contain "/watch?v=", meaning that it is a link to a viewable youtube video.
            var mostRelevantLink = documentRoot.DocumentNode.Descendants("a")
                                            .FirstOrDefault(link => link.Attributes["href"].Value.Contains("/watch?v="));
            result.fullDownloadLink = BASE_URL + mostRelevantLink.Attributes["href"].Value;

            // Find this link's song title (if not empty)
            foreach(var node in documentRoot.DocumentNode.Descendants("a"))
            {
                // Attention, some nodes could have no set class!!
                if (node.Attributes["class"] != null)
                {
                    if (node.Attributes["class"].Value.Contains("yt-uix-tile-link"))
                    {
                        // This node is the first most relevant one, get the title out of it and bail.
                        result.searchSuccess = true;
                        result.title = node.InnerText;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Downloads the song from youtube.
        /// </summary>
        /// <param name="info">The song's metadata, precedently found.</param>
        private void downloadAndConvertSong(FoundSongMeta info)
        {
            // Prepare the metadata for YoutubeExtractor lib
            var videoInfos = DownloadUrlResolver.GetDownloadUrls(info.fullDownloadLink);

            VideoInfo video = videoInfos
                .Where(vidInfo => vidInfo.CanExtractAudio)
                .OrderByDescending(vidInfo => vidInfo.AudioBitrate)
                .First();
            if(video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }

            //TODO: Let the user choose where to download the song, not just in the CWD.
            var audioDownloader = new AudioDownloader(video, video.Title + video.AudioExtension);

            // audioDownloader.DownloadProgressChanged += null;
            // audioDownloader.AudioExtractionProgressChanged += null;
            // audioDownloader.DownloadFinished += null;

            audioDownloader.Execute();
            //TODO: tell the user when the operation is done! (eg.: event system that the gui can register to)
        }
    }
}

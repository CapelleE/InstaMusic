﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstaMusic
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSearchNow_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            ResultWindow rw = new ResultWindow(songInput.Text);
            this.Show();
            resetInputs();
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            resetInputs();
        }

        private void resetInputs()
        {
            songInput.Text = "";
            songInput.Focus();
        }
    }
}
